package creditCalc;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class CreditCalculatorTest {

    @Test
    public void getCreditOffers_shouldReturnEmptyOptional_withTenureLessThanShortestLoan() {
        //given
        CreditCalculator creditCalculator = new CreditCalculator();
        int tooLowTenure = creditCalculator.getMinCreditPeriod() - 1;
        //when
        Optional<List<ICreditOffer>> creditOffers = creditCalculator.getCreditOffers(tooLowTenure, 100000, 0, 0, 0);
        //then
        assertTrue(creditOffers.isEmpty());
    }

    @Test
    public void getCreditOffers_shouldReturnEmptyOptional_withMaintenanceCostsGreaterThanMaxCreditShareInIncomePossible() {
        //given
        CreditCalculator creditCalculator = getCreditCalculator();
        //when
        Optional<List<ICreditOffer>> creditOffers = creditCalculator.getCreditOffers(12, 10000, 5501, 0, 0);
        //then
        assertTrue(creditOffers.isEmpty());
    }

    @Test
    public void getCreditOffers_shouldReturnAllOffers_withTenureGreaterThanMaxCreditDurationAndOtherFieldsOk() {
        //given
        CreditCalculator creditCalculator = getCreditCalculator();
        int tenure = 101;
        //when
        Optional<List<ICreditOffer>> creditOffers = creditCalculator.getCreditOffers(tenure, 10000, 0, 0, 0);
        //then
        assertEquals(2, creditOffers.get().size());
    }

    @Test
    public void getCreditOffers_shouldReturnOneOfferWithMaxPeriodEqualToTenure_withLessThanMaxPeriodForShortestDurationButLargerThanMin() {
        //given
        CreditCalculator creditCalculator = getCreditCalculator();
        int tenure = 10;
        //when
        Optional<List<ICreditOffer>> creditOffers = creditCalculator.getCreditOffers(tenure, 10000, 0, 0, 0);
        //then
        assertEquals(1, creditOffers.get().size());
        assertEquals(10, creditOffers.get().get(0).getMaxCreditDuration());
    }

    @Test
    public void getCreditOffer_shouldReturnOffersEqualToCalculatedBefore() {
        //given
        CreditCalculator creditCalculator = getCreditCalculator();
        //when
        List<ICreditOffer> creditOffers = creditCalculator.getCreditOffers(12, 10000, 2000, 0, 0).get();
        //then
        ICreditOffer creditOffer = creditOffers.get(0);
        double score = 25281.98;
        double maxInstallment = 5000;
        assertEquals(creditOffer.getMaxCreditAmount(), score);
        assertEquals(creditOffer.getMaxCreditInstallment(), maxInstallment);
    }

    @Test
    public void getCreditOffer_shouldReturnNoOffers_withMaxCreditAmountLessThanMin(){
        //given
        CreditCalculator creditCalculator = getCreditCalculator();
        double income = 50;
        double maintenance = 100;
        int tenure = 100;
        //when
        Optional<List<ICreditOffer>> creditOffers = creditCalculator.getCreditOffers(tenure, income, maintenance, 0, 0);
        //then
        assertTrue(creditOffers.isEmpty());
    }


    private CreditCalculator getCreditCalculator() {
        ICreditRepository creditRepository = new ICreditRepository() {
            @Override
            public int getMinPeriod() {
                return 6;
            }

            @Override
            public int getMaxPeriod() {
                return 100;
            }

            @Override
            public double getMaxCreditAmount() {
                return 150000;
            }

            @Override
            public double getMinCreditAmount() {
                return 5000;
            }

            @Override
            public double getMaxCommitment() {
                return 200000;
            }

            @Override
            public Optional<List<CreditParams>> getAllCreditParamsForGivenMaxPeriod(int maxPeriod) {
                List<CreditParams> params = new LinkedList<>();
                if (maxPeriod>6){
                    params.add(new CreditParams(50, 0.02, 6, 12));
                }
                if(maxPeriod>13){
                    params.add(new CreditParams(55, 0.03, 13, 36));
                }
                return Optional.of(params);
            }
        };
        return new CreditCalculator(creditRepository, new MaxInstallmentCalc(), new MaxCreditAmountCalc(creditRepository));
    }
}
