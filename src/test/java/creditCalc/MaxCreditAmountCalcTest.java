package creditCalc;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class MaxCreditAmountCalcTest {
    @Test
    public void getMaxCreditAmountForOffer_shouldReturnValueEqualToCalculatedBefore(){
        //given
        CreditParams creditParams = new CreditParams(50, 0.02, 6,12);
        double monthlyInstallment = 5000;
        IMaxCreditAmountCalc maxCreditAmountCalc = new MaxCreditAmountCalc(getMaxCreditAmountCalc());
        //when
        double maxCreditAmountForOffer = maxCreditAmountCalc.getMaxCreditAmountForOffer(creditParams, monthlyInstallment, 0, 0);
        //then
        assertEquals(25281.98, maxCreditAmountForOffer );
    }

    @Test
    public void getMaxCreditAmountFotOffer_shouldReturnMaxCommitmentAmountMinusCreditsSum_withMaxAmountLargerThanMaxCredAmount(){
        //given
        CreditParams creditParams = new CreditParams(50, 0.02, 6,12);
        double monthlyInstallment = 100000;
        double creditsSum = 60000;
        IMaxCreditAmountCalc maxCreditAmountCalc = new MaxCreditAmountCalc(getMaxCreditAmountCalc());
        //when
        double maxCreditAmountForOffer = maxCreditAmountCalc.getMaxCreditAmountForOffer(creditParams, monthlyInstallment, 0, creditsSum);
        //then
        assertEquals(140000, maxCreditAmountForOffer);
    }

    private ICreditRepository getMaxCreditAmountCalc() {
        return new ICreditRepository() {
            @Override
            public int getMinPeriod() {
                return 6;
            }

            @Override
            public int getMaxPeriod() {
                return 100;
            }

            @Override
            public double getMaxCreditAmount() {
                return 150000.00;
            }

            @Override
            public double getMinCreditAmount() {
                return 5000.00;
            }

            @Override
            public double getMaxCommitment() {
                return 200000.00;
            }

            @Override
            public Optional<List<CreditParams>> getAllCreditParamsForGivenMaxPeriod(int maxPeriod) {
                return Optional.empty();
            }
        };
    }
}
