package creditCalc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MaxInstallmentCalcTest {

    @Test
    public void getMaxInstallment_shouldReturnMaxInstallmentEqualToCalculatedBefore(){
        //given
        MaxInstallmentCalc maxInstallmentCalc = new MaxInstallmentCalc();
        double monthlyIncome = 5000;
        double upkeepCosts = 2000;
        double obligations = 500;
        double maxCreditShareOfIncome = 50;
        //when
        double maxInstallment = maxInstallmentCalc.getMaxInstallment(monthlyIncome, upkeepCosts, obligations, maxCreditShareOfIncome);
        //then
        Assertions.assertEquals(2000, maxInstallment);
    }

}
