package creditCalc;

class CreditParams {

    private final double maxCreditShareOfIncome;
    private final double interest;
    private final int minMonths;
    private final int maxMonths;

    public CreditParams(double maxCreditShareOfIncome, double interest, int minMonths, int maxMonths) {
        this.maxCreditShareOfIncome = maxCreditShareOfIncome;
        this.interest = interest;
        this.minMonths = minMonths;
        this.maxMonths = maxMonths;
    }

    double getMaxCreditShareOfIncome() {
        return maxCreditShareOfIncome;
    }

    double getInterest() {
        return interest;
    }

    public int getMinMonths() {
        return minMonths;
    }

    public int getMaxMonths() {
        return maxMonths;
    }

    boolean fits(int months){
        return minMonths < months;
    }
}
