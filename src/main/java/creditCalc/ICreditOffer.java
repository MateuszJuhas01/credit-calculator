package creditCalc;

public interface ICreditOffer {

    double getMaxCreditAmount();

    double getMaxCreditInstallment();

    int getMaxCreditDuration();
}
