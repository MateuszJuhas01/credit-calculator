package creditCalc;

import java.util.List;
import java.util.Optional;

interface ICreditRepository {
    int getMinPeriod();

    int getMaxPeriod();

    double getMaxCreditAmount();

    double getMinCreditAmount();

    double getMaxCommitment();

    Optional<List<CreditParams>> getAllCreditParamsForGivenMaxPeriod(int maxPeriod);

}
