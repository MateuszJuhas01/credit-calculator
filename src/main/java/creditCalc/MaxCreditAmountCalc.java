package creditCalc;

import java.math.BigDecimal;
import java.math.RoundingMode;

class MaxCreditAmountCalc implements IMaxCreditAmountCalc{

    private ICreditRepository creditRepository;

    public MaxCreditAmountCalc(ICreditRepository creditRepository) {
        this.creditRepository = creditRepository;
    }

    @Override
    public double getMaxCreditAmountForOffer(CreditParams creditParams, double monthlyInstallment, double obligations, double creditsSum, int maxPeriod) {
        double mi = (creditParams.getInterest()*100) / 12;
        double beforePower = (1 + mi);
        double pow = Math.pow(beforePower, -maxPeriod);
        double calculatedAmount = monthlyInstallment * ((1-pow) / mi);
        double valToReturn;
        if(calculatedAmount+obligations>creditRepository.getMaxCommitment()){
            valToReturn = Math.min(creditRepository.getMaxCommitment() - creditsSum, creditRepository.getMaxCreditAmount());
        }else {
            valToReturn = Math.min(creditRepository.getMaxCreditAmount(),calculatedAmount);
        }
        return round(valToReturn);
    }

    private double round(double doubleToRound) {
        BigDecimal bd = BigDecimal.valueOf(doubleToRound);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getMaxCreditAmountForOffer(CreditParams creditParams, double monthlyInstallment,  double obligations,  double creditsSum){
        return getMaxCreditAmountForOffer(creditParams, monthlyInstallment, obligations, creditsSum, creditParams.getMaxMonths());
    }
}
