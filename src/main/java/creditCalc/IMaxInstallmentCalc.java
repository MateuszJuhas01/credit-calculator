package creditCalc;

interface IMaxInstallmentCalc {
    double getMaxInstallment(double monthlyIncome, double upkeepCosts, double obligations, double maxCreditShareOfIncome);
}
