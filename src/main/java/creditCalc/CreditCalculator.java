package creditCalc;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CreditCalculator {

    private final ICreditRepository creditRepository;
    private final IMaxInstallmentCalc maxInstallmentCalc;
    private final IMaxCreditAmountCalc maxCreditAmountCalc;

    public CreditCalculator() {
        creditRepository = new CreditMemoryBase();
        maxCreditAmountCalc = new MaxCreditAmountCalc(creditRepository);
        maxInstallmentCalc = new MaxInstallmentCalc();
    }

    CreditCalculator(ICreditRepository creditMemoryBase, IMaxInstallmentCalc maxInstallmentCalc, IMaxCreditAmountCalc maxCreditAmountCalc) {
        this.creditRepository = creditMemoryBase;
        this.maxInstallmentCalc = maxInstallmentCalc;
        this.maxCreditAmountCalc = maxCreditAmountCalc;
    }

    public Optional<List<ICreditOffer>> getCreditOffers(final int tenure, final double monthlyIncome, final double maintenanceCosts, final double obligations, final double creditsSum){
        Optional<List<CreditParams>> creditParamsForTenure = creditRepository.getAllCreditParamsForGivenMaxPeriod(tenure);
        if(creditParamsForTenure.isEmpty()){
            return Optional.empty();
        }
        int maxPeriod = creditRepository.getMaxPeriod();
        if (maxPeriod>tenure){
            maxPeriod = tenure;
        }
        int finalMaxPeriod = maxPeriod;
        List<ICreditOffer> collect = creditParamsForTenure.get().stream().map(p-> createCreditOffer(p, finalMaxPeriod, monthlyIncome, maintenanceCosts, obligations, creditsSum)).
        filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
        if(collect.isEmpty()){
            return Optional.empty();
        }else {
            return Optional.of(collect);
        }
    }

    private Optional<ICreditOffer> createCreditOffer(CreditParams creditParams, int maxPeriod, double income, double monthlyIncome, double upkeepCosts, double obligations) {
        if((upkeepCosts/monthlyIncome)>creditParams.getMaxCreditShareOfIncome()/100){
            return Optional.empty();
        }
        CreditOffer.Builder builder = new CreditOffer.Builder();
        if(creditParams.getMaxMonths()<maxPeriod){
            maxPeriod = creditParams.getMaxMonths();
        }
        if (maxPeriod<creditRepository.getMinPeriod()){
            return Optional.empty();
        }
        builder.withMaxCreditDuration(maxPeriod);
        double maxInstallment = maxInstallmentCalc.getMaxInstallment(monthlyIncome, upkeepCosts, obligations, creditParams.getMaxCreditShareOfIncome());
        builder.withMaxCreditInstallment(maxInstallment);
        double maxCreditAmountForOffer = maxCreditAmountCalc.getMaxCreditAmountForOffer(creditParams, maxInstallment, obligations,  maxPeriod);
        if(maxCreditAmountForOffer<creditRepository.getMinPeriod()){
            return Optional.empty();
        }
        builder.withMaxCreditAmount(maxCreditAmountForOffer);
        return Optional.of(builder.build());
    }

    int getMinCreditPeriod(){
        return creditRepository.getMinPeriod();
    }





}
