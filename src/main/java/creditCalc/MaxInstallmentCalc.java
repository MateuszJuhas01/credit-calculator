package creditCalc;

class MaxInstallmentCalc implements IMaxInstallmentCalc{
    @Override
    public double getMaxInstallment(double monthlyIncome, double upkeepCosts, double obligations, double maxCreditShareOfIncome) {
        double maxInstallment1 = monthlyIncome - upkeepCosts - obligations;
        double maxInstallment2 = ((maxCreditShareOfIncome/100) * monthlyIncome) - obligations;
        return Math.min(maxInstallment1, maxInstallment2);
    }
}
