package creditCalc;

class CreditOffer implements ICreditOffer{

    private final double maxCreditAmount;
    private final double maxCreditInstallment;
    private final int maxCreditDuration;

    private CreditOffer(double maxCreditAmount, double maxCreditInstallment, int maxCreditDuration) {
        this.maxCreditAmount = maxCreditAmount;
        this.maxCreditInstallment = maxCreditInstallment;
        this.maxCreditDuration = maxCreditDuration;
    }

    @Override
    public double getMaxCreditAmount() {
        return maxCreditAmount;
    }

    @Override
    public double getMaxCreditInstallment() {
        return maxCreditInstallment;
    }

    @Override
    public int getMaxCreditDuration() {
        return maxCreditDuration;
    }

    public static class Builder{
        private double maxCreditAmount;
        private double maxCreditInstallment;
        private int maxCreditDuration;

        public Builder withMaxCreditAmount(double maxCreditAmount){
            this.maxCreditAmount=maxCreditAmount;
            return this;
        }

        public Builder withMaxCreditInstallment(double maxCreditInstallment){
            this.maxCreditInstallment = maxCreditInstallment;
            return this;
        }

        public Builder withMaxCreditDuration(int maxCreditDuration){
            this.maxCreditDuration = maxCreditDuration;
            return this;
        }

        public CreditOffer build(){
            return new CreditOffer(maxCreditAmount, maxCreditInstallment, maxCreditDuration);
        }
    }
}
