package creditCalc;

import java.util.*;
import java.util.stream.Collectors;

class CreditMemoryBase implements ICreditRepository {

    private static final double MAX_CREDIT_AMOUNT = 150000.00;
    private static final double MAX_COMMITMENT = 200000.00;
    private static final double MIN_CREDIT_AMOUNT = 5000.00;
    private static final int MIN_PERIOD = 6;
    private static final int MAX_PERIOD = 100;

    private final Set<CreditParams> creditParamsSet;

    CreditMemoryBase() {
        creditParamsSet = new HashSet<>();
        CreditParams sixMonthParam = new CreditParams(0.6, 0.02, 6, 12);
        CreditParams thirteenMonthParam = new CreditParams(0.6, 0.03, 13, 36);
        CreditParams thirtySevenMonthParam = new CreditParams(0.5, 0.03, 37, 60);
        CreditParams sixtyOneMonthParam = new CreditParams(0.55, 0.03, 61, 100);
        creditParamsSet.add(sixMonthParam);
        creditParamsSet.add(thirteenMonthParam);
        creditParamsSet.add(thirtySevenMonthParam);
        creditParamsSet.add(sixtyOneMonthParam);
    }

    public int getMinPeriod(){
        return MIN_PERIOD;
    }

    public int getMaxPeriod(){
        return MAX_PERIOD;
    }

    public double getMaxCreditAmount(){
        return MAX_CREDIT_AMOUNT;
    }

    public double getMinCreditAmount(){
        return MIN_CREDIT_AMOUNT;
    }

    public double getMaxCommitment(){
        return MAX_COMMITMENT;
    }

    @Override
    public Optional<List<CreditParams>> getAllCreditParamsForGivenMaxPeriod(int maxPeriod) {
        return Optional.of(creditParamsSet.stream().filter(e -> e.fits(maxPeriod)).collect(Collectors.toList()));
    }

}
