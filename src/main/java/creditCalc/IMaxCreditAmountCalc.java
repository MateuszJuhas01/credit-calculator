package creditCalc;

interface IMaxCreditAmountCalc {
    double getMaxCreditAmountForOffer(CreditParams creditParams, double monthlyInstallment, double obligations, double creditsSum, int maxPeriod);

    double getMaxCreditAmountForOffer(CreditParams creditParams, double monthlyInstallment,  double obligations, double creditsSum);
}
