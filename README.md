# This project shares method to calculte max avalible credit amount. 

### Class CreditCalculator can be created via no-arg constructor, and haves only one public method which takes parameters:
int tenure - its time period of being in work, max credit duration is based on this
* double monthlyIncome 
* double maintenanceCosts - monthly costs of living
* duble  obligations - sum of credit commitments
* double creditsSum - sum of unpaid credits taken

returns empty optional if there is no offers or optional of CreditOffert list.
### Credit offer has three methods:
 * double getMaxCreditAmount() - max credit amount calculated for thiss offer 
 * double getMaxCreditInstallment() - max monthly installment for this offer
 * int getMaxCreditDuration() - max credit duration, this is max for this offer or max possible duration calculated if its smaller than max duration fot offer
